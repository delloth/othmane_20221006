import {useEffect, useState} from "react";
import {Button, Container, Form, Nav, Navbar} from "react-bootstrap";
import {Category} from "./types";


export const VideoUpload = () => {

    const validVideoTypes = ['mp4', 'move']
    const [video, setVideo] = useState();
    const [title, setTitle] = useState('');
    const [category, setCategory] = useState('1');
    const [categoryList, setCategoryList] = useState([]);
    const url = 'http://localhost:3000/videos'

    useEffect(()=> {fetchList()},[])
    const fetchList = () => {
        fetch('http://localhost:3000/categories')
            .then((response) => response.json())
            .then((data) => {
                setCategoryList(data as any)
            })
    }
    const handleFileInput = (e: any) => {
        const video = e.target.files[0];
        const fileEx = video.name.split('.').pop()

        if (video.size > 2e+8  ){
            alert("Video file is bigger tha the maximum 200mb")
        }
        else{
            if (!validVideoTypes.includes(fileEx)){
                alert("File type not supported")
            }
            else{
                setVideo(video);
            }}}

    const submitForm = (e: any) => {
        e.preventDefault()
        const data = new FormData();
        data.append("video[title]", title);
        data.append("video[category_id]", category);
        data.append("video[video_clip]", (video as any));

        fetch(url, {
            method: "POST",
            body: data,
        }).then((response) => response.json())
          .then(() => alert("Video Upload success"))
          .catch((error) => console.error(error));
    };
return(
    <div >
        <Navbar bg="primary" variant="dark">
            <Container>
                <Navbar.Brand ></Navbar.Brand>
                <Nav className="me-auto">
                    <Nav.Link href="/">back to video list</Nav.Link>
                </Nav>
            </Container>
        </Navbar>
        <Container>
            <Form onSubmit={(e: any) =>submitForm(e)} className='mg-auto'>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" placeholder="Enter title" onChange={(e: any) => {setTitle(e.target.value)}} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Category</Form.Label>
                    <Form.Select onChange={(e: any) => {setCategory(e.target.value)}}>
                        {categoryList.map((category: Category) => (
                            <option key={category.id} value={category.id}>
                                {category.title}
                            </option>
                        ))}
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" >
                    <Form.Label>Insert video file</Form.Label>
                    <Form.Control type="file" onChange={(e: any) => {handleFileInput(e)}} />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </Container>
    </div>
)
}