export type Video = {
    id:string
    type:string
    attributes:{
        title:string
        category:string
        video_clip_url:string
        thumbnail:string
    }
}

export type Category = {
    id:string
    title:string
}
export type CategoryList = Category []

export type VideoProps = { video:Video }