import {useEffect, useState} from "react";
import {VideoView} from "./VideoView";
import {Button, Col, Container, Nav, Navbar, Row} from "react-bootstrap";
import {Video} from "./types";

export const VideoList = () => {

    const [videoList, setVideoList] = useState([])
    const url = 'http://localhost:3000/videos'
    useEffect(()=> {fetchList()},[])
    const fetchList = () => {
        fetch(url)
            .then((response) => response.json())
            .then((data) => {
                setVideoList(data as any)
                console.log(data)
            })
            .catch((error) => console.error(error));
    }

    return (
            <div >
                <Navbar bg="primary" variant="dark">
                    <Container>
                        <Navbar.Brand ></Navbar.Brand>
                        <Nav className="me-auto">
                            <Button variant="dark" type="submit">
                                <Nav.Link href="/new">Upload</Nav.Link>
                            </Button>

                        </Nav>
                    </Container>
                </Navbar>
                <Container>
                    <Row className='my-3'>

                            {videoList.map((video: Video) =>
                                <Col sm={3} className='me-3' key={video.id}>
                                <VideoView video={video} />
                                </Col>
                            )}

                    </Row>
                </Container>
            </div>

    )
}