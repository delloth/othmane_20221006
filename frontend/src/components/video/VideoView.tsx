import {Video, VideoProps} from "./types";
import {Button, Card} from "react-bootstrap";



export const VideoView = (props: VideoProps) => {

const handleVideoPlay = (video: any) =>{
    video.paused? video.play(): video.pause();
}
return (
    <Card  className="mb-3" style={{ width: '256px'} } key={props.video.id}>
        <video  poster={props.video.attributes.thumbnail}
                src={props.video.attributes.video_clip_url}
                onClick={(e: any) => handleVideoPlay(e.target)}
                onMouseOver={() => {document.title = props.video.attributes.title}}
        ></video>
        <Card.Body>
            <Card.Title>{props.video.attributes.title}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{props.video.attributes.category}</Card.Subtitle>
        </Card.Body>
    </Card>
)
}