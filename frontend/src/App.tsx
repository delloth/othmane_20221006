import {VideoList} from "./components/video/VideoList";
import {VideoUpload} from "./components/video/VideoUpload";
import {Route, Routes,  BrowserRouter} from 'react-router-dom'

function App() {
  return (

    <div className="App">
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<VideoList />} />
                <Route path="/new" element={<VideoUpload />} />
            </Routes>
        </BrowserRouter>
    </div>
  );
}

export default App;
