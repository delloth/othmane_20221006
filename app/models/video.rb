class Video < ApplicationRecord
  #Relations
  belongs_to :category
  has_one_attached :video_clip, dependent: :destroy

  #Validation
  validates :title, :category_id, presence: true
  validate :validate_video_type
  validate :validate_video_size

  #Callbacks
  after_save :generate_previews

  def thumbnail
    video_clip.variant(:thumb_bg)
  end

  def video_clip_url
    Rails.application.routes.url_helpers.url_for(video_clip) if video_clip.attached?
  end

  private

  def validate_video_type
    return unless video_clip.attached?
    unless video_clip.content_type.in?(%w[video/mp4 video/mov quicktime])
      errors.add(:video_clip, 'file type not supported')
    end
  end

  def validate_video_size
    return unless video_clip.attached?
    if video_clip.byte_size > 200.megabytes
      errors.add(:video_clip, 'file too big')
    end
  end


  def generate_previews
    VideoPreviewJob.perform_later(self)
  end
end
