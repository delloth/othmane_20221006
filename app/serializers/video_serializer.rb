class VideoSerializer
  include JSONAPI::Serializer

  attributes :title, :video_clip_url

  attribute :category do |video|
    video.category&.title
  end

  attribute :thumbnail do |video|
    ActiveStorage::Current.set(host: "http://localhost:3000") do
      video.video_clip.previewable? ? video.video_clip.preview(resize_to_limit: [256,256]).processed.url : ''
    end
  end
end