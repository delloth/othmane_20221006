# README

This README would normally document whatever steps are necessary to get the
application up and running.


* Ruby version
  * 3.1.2

* System dependencies:
  * postgresql
  * ffmpeg (this needs to be installed)

* Setup:
  * bundle install 
  * cd frontend, npm install

* Database creation
  * start postgresql
  * rails db:setup

* Database initialization
  * rails db:seed

* Running the app
  * rails s
  * cd frontend, npm run start



